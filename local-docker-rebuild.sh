#stop docker containers from running
docker kill $(docker ps -q)

#build docker images
docker build -f Dockerfile -t docker-airflow  .
# docker build --no-cache -f Dockerfile -t docker-airflow  .

echo "docker-airflow image ready."
echo "Starting docker-airflow container."

#start docker images back up
docker-compose -f docker-compose.yaml --env-file env/local up -d