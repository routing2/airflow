from airflow.models import Variable

prior_env = Variable.get("env")
Variable.set(key='env', value='local', serialize_json=False)
new_env = Variable.get("env")
print(f'airflow variable "env": {prior_env} to {new_env}...')