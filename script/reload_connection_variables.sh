# copy table ddl from aws (public.connection & public.variable)
# store ddl as files (connection.sql & variable.sql)

pg_dump --dbname=postgresql://$(aws secretsmanager get-secret-value --profile PowerPlusUser-458548934557 --region us-east-1 --secret-id airflow-rds --query 'SecretString' --output text | jq .username | tr -d '"'):$(aws secretsmanager get-secret-value --profile PowerPlusUser-458548934557 --region us-east-1 --secret-id airflow-rds --query 'SecretString' --output text | jq .password | tr -d '"')@$(aws secretsmanager get-secret-value --profile PowerPlusUser-458548934557 --region us-east-1 --secret-id airflow-rds --query 'SecretString' --output text | jq .host | tr -d '"'):5432/airflow --table public.connection > connection.sql
pg_dump --dbname=postgresql://$(aws secretsmanager get-secret-value --profile PowerPlusUser-458548934557 --region us-east-1 --secret-id airflow-rds --query 'SecretString' --output text | jq .username | tr -d '"'):$(aws secretsmanager get-secret-value --profile PowerPlusUser-458548934557 --region us-east-1 --secret-id airflow-rds --query 'SecretString' --output text | jq .password | tr -d '"')@$(aws secretsmanager get-secret-value --profile PowerPlusUser-458548934557 --region us-east-1 --secret-id airflow-rds --query 'SecretString' --output text | jq .host | tr -d '"'):5432/airflow --table public.variable > variable.sql
pg_dump --dbname=postgresql://$(aws secretsmanager get-secret-value --profile PowerPlusUser-458548934557 --region us-east-1 --secret-id airflow-rds --query 'SecretString' --output text | jq .username | tr -d '"'):$(aws secretsmanager get-secret-value --profile PowerPlusUser-458548934557 --region us-east-1 --secret-id airflow-rds --query 'SecretString' --output text | jq .password | tr -d '"')@$(aws secretsmanager get-secret-value --profile PowerPlusUser-458548934557 --region us-east-1 --secret-id airflow-rds --query 'SecretString' --output text | jq .host | tr -d '"'):5432/airflow --table public.ab_user_role > ab_user_role.sql
pg_dump --dbname=postgresql://$(aws secretsmanager get-secret-value --profile PowerPlusUser-458548934557 --region us-east-1 --secret-id airflow-rds --query 'SecretString' --output text | jq .username | tr -d '"'):$(aws secretsmanager get-secret-value --profile PowerPlusUser-458548934557 --region us-east-1 --secret-id airflow-rds --query 'SecretString' --output text | jq .password | tr -d '"')@$(aws secretsmanager get-secret-value --profile PowerPlusUser-458548934557 --region us-east-1 --secret-id airflow-rds --query 'SecretString' --output text | jq .host | tr -d '"'):5432/airflow --table public.ab_user > ab_user.sql

# drop tables
psql --dbname postgresql://airflow:airflow@localhost:5432/airflow -c "drop table if exists public.connection"
psql --dbname postgresql://airflow:airflow@localhost:5432/airflow -c "drop table if exists public.variable"
psql --dbname postgresql://airflow:airflow@localhost:5432/airflow -c "drop table if exists public.ab_user_role"
psql --dbname postgresql://airflow:airflow@localhost:5432/airflow -c "drop table if exists public.ab_user"

# run ddl files (connection.sql & variable.sql)
psql --dbname postgresql://airflow:airflow@localhost:5432/airflow -f connection.sql
psql --dbname postgresql://airflow:airflow@localhost:5432/airflow -f variable.sql
psql --dbname postgresql://airflow:airflow@localhost:5432/airflow -f ab_user_role.sql
psql --dbname postgresql://airflow:airflow@localhost:5432/airflow -f ab_user.sql

# create auto-increment for variable 
psql --dbname postgresql://airflow:airflow@localhost:5432/airflow -c "CREATE SEQUENCE variable_id_seq OWNED BY variable.id;";
psql --dbname postgresql://airflow:airflow@localhost:5432/airflow -c "SELECT setval('public.variable_id_seq', coalesce(max(id), 0) + 1, false) FROM public.variable;";
psql --dbname postgresql://airflow:airflow@localhost:5432/airflow -c "ALTER TABLE ONLY public.variable ALTER COLUMN id SET DEFAULT nextval('public.variable_id_seq'::regclass);";

# create auto-increment for connection 
psql --dbname postgresql://airflow:airflow@localhost:5432/airflow -c "CREATE SEQUENCE connection_id_seq OWNED BY connection.id;";
psql --dbname postgresql://airflow:airflow@localhost:5432/airflow -c "SELECT setval('public.connection_id_seq', coalesce(max(id), 0) + 1, false) FROM public.connection;";
psql --dbname postgresql://airflow:airflow@localhost:5432/airflow -c "ALTER TABLE ONLY public.connection ALTER COLUMN id SET DEFAULT nextval('public.connection_id_seq'::regclass);";

#remove .sql files
rm connection.sql
rm variable.sql
rm ab_user.sql
rm ab_user_role.sql

#set environment variable to 'local'
docker exec -it airflow_webserver_1 python local_variable.py